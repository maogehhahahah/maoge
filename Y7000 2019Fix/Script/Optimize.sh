#!/bin/bash

echo "
************************************************************************************
                                                  
          6666666666666
                                                                                  
************************************************************************************
"

DAEMON_PATH=/Library/LaunchDaemons/
ALC_CONFIG_PATH=/Library/Preferences/ALCPlugFix/
BIN_PATH=/usr/local/bin/
TMP_PATH=/tmp/
ALC_DAEMON_FILE=com.black-dragon74.ALCPlugFix.plist
ALC_FIX_FILE=ALCPlugFix
ALC_FIX_CONFIG=ALC_Config.plist

echo "This script required to run as root"

sudo spctl --master-disable

sudo pmset -a hibernatemode 0

sudo rm -rf /var/vm/sleepimage

sudo mkdir /var/vm/sleepimage

echo "Downloading required file"
sudo curl -o $TMP_PATH$ALC_FIX_FILE "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000%202019Fix/ALCPlugFix/$ALC_FIX_FILE"
sudo curl -o $TMP_PATH$ALC_DAEMON_FILE "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000%202019Fix/ALCPlugFix/$ALC_DAEMON_FILE"
sudo curl -o $TMP_PATH$ALC_FIX_CONFIG "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000%202019Fix/ALCPlugFix/$ALC_FIX_CONFIG"


if sudo launchctl list | grep --quiet good.win.ALCPlugFix.plist; then
    sudo launchctl unload /Library/LaunchDaemons/good.win.ALCPlugFix.plist
	sudo rm /Library/LaunchDaemons/good.win.ALCPlugFix.plist
	sudo rm /usr/local/bin/hda-verb
fi

if [ ! -d "$BIN_PATH" ] ; then
    mkdir $BIN_PATH
fi

if [ ! -d "$ALC_CONFIG_PATH" ] ; then
	sudo mkdir $ALC_CONFIG_PATH
fi

echo "Copy file to destination place..."
sudo cp $TMP_PATH$ALC_FIX_FILE $BIN_PATH
sudo cp $TMP_PATH$ALC_DAEMON_FILE $DAEMON_PATH
sudo cp $TMP_PATH$ALC_FIX_CONFIG $ALC_CONFIG_PATH
sudo rm $TMP_PATH$ALC_FIX_FILE
sudo rm $TMP_PATH$ALC_DAEMON_FILE
sudo rm $TMP_PATH$ALC_FIX_CONFIG_FILE

echo "Chmod ALCPlugFix..."
sudo chmod 755 $BIN_PATH$ALC_FIX_FILE
sudo chown root:wheel $BIN_PATH$ALC_FIX_FILE
sudo chmod 644 $DAEMON_PATH$ALC_DAEMON_FILE
sudo chown root:wheel $DAEMON_PATH$ALC_DAEMON_FILE
sudo chmod 644 $ALC_CONFIG_PATH$ALC_FIX_CONFIG

echo "rest kextcache..."
sudo kextcache -i /

echo "All Done!"
exit 0
