#!/bin/bash

echo "
************************************************************************************
                                    
                                      
                                    Maoge Hackintosh

                                    Have a good time！

                                                                                  
************************************************************************************
"

DAEMON_PATH=/Library/LaunchDaemons/
BIN_PATH=/usr/local/bin/
TMP_PATH=/tmp/
ALC_DAEMON_FILE=good.win.ALCPlugFix.plist
VERB_FILE=hda-verb
ALC_FIX_FILE=ALCPlugFix

echo "This script required to run as root"

sudo spctl --master-disable

sudo pmset -a hibernatemode 0

sudo rm -rf /var/vm/sleepimage

sudo mkdir /var/vm/sleepimage

echo "Downloading required file"
sudo curl -o $TMP_PATH$ALC_FIX_FILE "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000Series_Hackintosh_Fix/ALCPlugFix/ALCPlugFix"
sudo curl -o $TMP_PATH$VERB_FILE "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000Series_Hackintosh_Fix/ALCPlugFix/hda-verb"
sudo curl -o $TMP_PATH$ALC_DAEMON_FILE "https://gitee.com/maogehhahahah/maoge/raw/master/Y7000Series_Hackintosh_Fix/ALCPlugFix/good.win.ALCPlugFix.plist"

if [ ! -d "$BIN_PATH" ] ; then
    mkdir "$BIN_PATH" ;
fi

echo "Copy file to destination place..."
sudo cp -a $TMP_PATH$ALC_FIX_FILE $BIN_PATH
sudo cp -a $TMP_PATH$VERB_FILE $BIN_PATH
sudo cp -a $TMP_PATH$ALC_DAEMON_FILE $DAEMON_PATH
sudo rm $TMP_PATH$ALC_FIX_FILE
sudo rm $TMP_PATH$VERB_FILE
sudo rm $TMP_PATH$ALC_DAEMON_FILE

echo "Chmod ALCPlugFix..."
sudo chmod 755 $BIN_PATH$ALC_FIX_FILE
sudo chown $USER:admin $BIN_PATH$ALC_FIX_FILE
sudo chmod 755 $BIN_PATH$VERB_FILE
sudo chown $USER:admin $BIN_PATH$VERB_FILE
sudo chmod 644 $DAEMON_PATH$ALC_DAEMON_FILE
sudo chown root:wheel $DAEMON_PATH$ALC_DAEMON_FILE

echo "Load ALCPlugFix..."
if sudo launchctl list | grep --quiet ALCPlugFix; then
    echo "Stopping existing ALCPlugFix daemon."
    sudo launchctl unload $DAEMON_PATH$ALC_DAEMON_FILE
fi
sudo launchctl load -w $DAEMON_PATH$ALC_DAEMON_FILE

echo "Rebuild the Kextcache..."
sudo kextcache -i /

echo "All done enjoy!!!"

exit 0
